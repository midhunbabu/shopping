<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblOrderDetail;

/**
 * OrderDetailSearch represents the model behind the search form about `app\models\TblOrderDetail`.
 */
class OrderDetailSearch extends TblOrderDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_int_order_detail_id', 'fk_int_order_id', 'fk_int_product_id', 'int_quantity'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblOrderDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pk_int_order_detail_id' => $this->pk_int_order_detail_id,
            'fk_int_order_id' => $this->fk_int_order_id,
            'fk_int_product_id' => $this->fk_int_product_id,
            'int_quantity' => $this->int_quantity,
        ]);

        return $dataProvider;
    }
}

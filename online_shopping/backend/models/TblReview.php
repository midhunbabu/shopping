<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_review".
 *
 * @property integer $pk_int_review_id
 * @property integer $fk_int_user_id
 * @property integer $fk_int_product_id
 * @property string $text_review_heading
 * @property string $text_review
 * @property integer $int_rating
 *
 * @property User $fkIntUser
 * @property TblProduct $fkIntProduct
 */
class TblReview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_int_user_id', 'fk_int_product_id', 'text_review_heading', 'text_review', 'int_rating'], 'required'],
            [['fk_int_user_id', 'fk_int_product_id', 'int_rating'], 'integer'],
            [['text_review_heading', 'text_review'], 'string'],
            [['fk_int_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_int_user_id' => 'id']],
            [['fk_int_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProduct::className(), 'targetAttribute' => ['fk_int_product_id' => 'pk_product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_int_review_id' => 'Pk Int Review ID',
            'fk_int_user_id' => 'Fk Int User ID',
            'fk_int_product_id' => 'Fk Int Product ID',
            'text_review_heading' => 'Text Review Heading',
            'text_review' => 'Text Review',
            'int_rating' => 'Int Rating',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntUser()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_int_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntProduct()
    {
        return $this->hasOne(TblProduct::className(), ['pk_product_id' => 'fk_int_product_id']);
    }
}

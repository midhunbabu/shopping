<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_order_detail".
 *
 * @property integer $pk_int_order_detail_id
 * @property integer $fk_int_order_id
 * @property integer $fk_int_product_id
 * @property integer $int_quantity
 */
class TblOrderDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_int_order_id', 'fk_int_product_id', 'int_quantity'], 'required'],
            [['fk_int_order_id', 'fk_int_product_id', 'int_quantity'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_int_order_detail_id' => 'Pk Int Order Detail ID',
            'fk_int_order_id' => 'Fk Int Order ID',
            'fk_int_product_id' => 'Fk Int Product ID',
            'int_quantity' => 'Int Quantity',
        ];
    }
}

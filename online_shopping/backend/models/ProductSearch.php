<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblProduct;

/**
 * ProductSearch represents the model behind the search form about `backend\models\TblProduct`.
 */
class ProductSearch extends TblProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_product_id', 'int_price'], 'integer'],
            [['vchr_product_name', 'fk_category_int_id', 'fk_int_sub_category_id', 'vchr_product_image', 'vchr_brand_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('fkCategoryInt');
        $query->joinWith('fkIntSubCategory');
        // grid filtering conditions
        $query->andFilterWhere([
            'pk_product_id' => $this->pk_product_id,
            'int_price' => $this->int_price,
        ]);

        $query->andFilterWhere(['like', 'vchr_product_name', $this->vchr_product_name])
            ->andFilterWhere(['like', 'vchr_product_image', $this->vchr_product_image])
            ->andFilterWhere(['like', 'vchr_brand_name', $this->vchr_brand_name])
            ->andFilterWhere(['like', 'tbl_category.vchr_category_name', $this->fk_category_int_id])
            ->andFilterWhere(['like', 'tbl_sub_category.vchr_sub_category_name', $this->fk_int_sub_category_id]);

        return $dataProvider;
    }
}

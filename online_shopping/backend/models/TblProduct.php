<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_product".
 *
 * @property integer $pk_product_id
 * @property integer $fk_category_int_id
 * @property integer $fk_int_sub_category_id
 * @property string $vchr_product_name
 * @property integer $int_price
 * @property string $vchr_product_image
 * @property string $vchr_brand_name
 *
 * @property TblSubCategory $fkIntSubCategory
 * @property TblCategory $fkCategoryInt
 * @property TblProductSize[] $tblProductSizes
 * @property TblReview[] $tblReviews
 */
class TblProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $size;
    public static function tableName()
    {
        return 'tbl_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_category_int_id', 'fk_int_sub_category_id', 'vchr_product_name', 'int_price', 'vchr_product_image', 'vchr_brand_name','size'], 'required'],
            [['fk_category_int_id', 'fk_int_sub_category_id', 'int_price'], 'integer'],
            [['vchr_product_name'], 'string', 'max' => 50],
            [['vchr_product_image'], 'file','skipOnEmpty' => false],
            [['vchr_brand_name'], 'string', 'max' => 30],
            [['fk_int_sub_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblSubCategory::className(), 'targetAttribute' => ['fk_int_sub_category_id' => 'pk_sub_category_id']],
            [['fk_category_int_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblCategory::className(), 'targetAttribute' => ['fk_category_int_id' => 'pk_category_id']],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_product_id' => ' Product ID',
            'fk_category_int_id' => ' Category',
            'fk_int_sub_category_id' => ' Sub Category ',
            'vchr_product_name' => 'Product Name',
            'int_price' => 'Price',
            'vchr_product_image' => ' Product Image',
            'vchr_brand_name' => ' Brand Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntSubCategory()
    {
        return $this->hasOne(TblSubCategory::className(), ['pk_sub_category_id' => 'fk_int_sub_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategoryInt()
    {
        return $this->hasOne(TblCategory::className(), ['pk_category_id' => 'fk_category_int_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductSizes()
    {
        return $this->hasMany(TblProductSize::className(), ['fk_int_product_id' => 'pk_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblReviews()
    {
        return $this->hasMany(TblReview::className(), ['fk_int_product_id' => 'pk_product_id']);
    }

    public function insertData($categoryid,$subcategory,$productname,$productimage,$brandname,$price)
    {   
       
        $this->fk_category_int_id = $categoryid;
        $this->fk_int_sub_category_id = $subcategory;
        $this->vchr_product_name = $productname;
        $this->vchr_product_image = $productimage;
        $this->vchr_brand_name = $brandname;
        $this->int_price = $price;
        
        $this->save();
    }    
}

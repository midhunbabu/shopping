<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblCustomers;

/**
 * CustomersSearch represents the model behind the search form about `app\models\TblCustomers`.
 */
class CustomersSearch extends TblCustomers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_int_customer_id'], 'integer'],
            [['vchr_name', 'vchr_gender', 'vchr_email', 'vchr_mobile_no', 'text_address', 'vchr_password'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblCustomers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pk_int_customer_id' => $this->pk_int_customer_id,
        ]);

        $query->andFilterWhere(['like', 'vchr_name', $this->vchr_name])
            ->andFilterWhere(['like', 'vchr_gender', $this->vchr_gender])
            ->andFilterWhere(['like', 'vchr_email', $this->vchr_email])
            ->andFilterWhere(['like', 'vchr_mobile_no', $this->vchr_mobile_no])
            ->andFilterWhere(['like', 'text_address', $this->text_address])
            ->andFilterWhere(['like', 'vchr_password', $this->vchr_password]);

        return $dataProvider;
    }
}

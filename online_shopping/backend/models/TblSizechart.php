<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_sizechart".
 *
 * @property integer $pk_int_sizeid
 * @property string $vchr_size
 *
 * @property TblProductSize[] $tblProductSizes
 */
class TblSizechart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sizechart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vchr_size'], 'required'],
            [['vchr_size'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_int_sizeid' => 'Pk Int Sizeid',
            'vchr_size' => 'Vchr Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductSizes()
    {
        return $this->hasMany(TblProductSize::className(), ['fk_int_size_id' => 'pk_int_sizeid']);
    }
}

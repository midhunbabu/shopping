<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblProductSize;

/**
 * ProductsizeSearch represents the model behind the search form about `app\models\TblProductSize`.
 */
class ProductsizeSearch extends TblProductSize
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_size_id', 'fk_item_id'], 'integer'],
            [['vchr_size'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblProductSize::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pk_size_id' => $this->pk_size_id,
            'fk_item_id' => $this->fk_item_id,
        ]);

        $query->andFilterWhere(['like', 'vchr_size', $this->vchr_size]);

        return $dataProvider;
    }
}

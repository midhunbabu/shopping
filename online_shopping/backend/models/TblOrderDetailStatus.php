<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_order_detail_status".
 *
 * @property integer $pk_int_order_status_id
 * @property integer $fk_int_order_id
 * @property integer $fk_int_order_detail_id
 * @property integer $fk_int_status_id
 * @property string $date_date
 */
class TblOrderDetailStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order_detail_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_int_order_id', 'fk_int_order_detail_id', 'fk_int_status_id', 'date_date'], 'required'],
            [['fk_int_order_id', 'fk_int_order_detail_id', 'fk_int_status_id'], 'integer'],
            [['date_date'], 'safe'],
            [['fk_int_order_detail_id'], 'unique'],
            [['fk_int_status_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_int_order_status_id' => 'Pk Int Order Status ID',
            'fk_int_order_id' => 'Fk Int Order ID',
            'fk_int_order_detail_id' => 'Fk Int Order Detail ID',
            'fk_int_status_id' => 'Fk Int Status ID',
            'date_date' => 'Date Date',
        ];
    }
}

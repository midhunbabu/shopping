<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_product_size".
 *
 * @property integer $pk_size_id
 * @property integer $fk_int_product_id
 * @property integer $fk_int_size_id
 *
 * @property TblSizechart $fkIntSize
 * @property TblProduct $fkIntProduct
 */
class TblProductSize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_product_size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_int_product_id', 'fk_int_size_id'], 'required'],
            [['fk_int_product_id', 'fk_int_size_id'], 'integer'],
            [['fk_int_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblSizechart::className(), 'targetAttribute' => ['fk_int_size_id' => 'pk_int_sizeid']],
            [['fk_int_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProduct::className(), 'targetAttribute' => ['fk_int_product_id' => 'pk_product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_size_id' => 'Pk Size ID',
            'fk_int_product_id' => 'Fk Int Product ID',
            'fk_int_size_id' => 'Fk Int Size ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntSize()
    {
        return $this->hasOne(TblSizechart::className(), ['pk_int_sizeid' => 'fk_int_size_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntProduct()
    {
        return $this->hasOne(TblProduct::className(), ['pk_product_id' => 'fk_int_product_id']);
    }
}

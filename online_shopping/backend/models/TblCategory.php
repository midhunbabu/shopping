<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_category".
 *
 * @property integer $pk_category_id
 * @property string $vchr_category_name
 *
 * @property TblProduct[] $tblProducts
 */
class TblCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vchr_category_name'], 'required'],
            [['vchr_category_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_category_id' => 'Pk Category ID',
            'vchr_category_name' => 'Vchr Category Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProducts()
    {
        return $this->hasMany(TblProduct::className(), ['fk_category_int_id' => 'pk_category_id']);
    }
}

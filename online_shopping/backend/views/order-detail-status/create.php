<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblOrderDetailStatus */

$this->title = 'Create Tbl Order Detail Status';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Order Detail Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-order-detail-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

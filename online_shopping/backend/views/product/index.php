<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">

  <div class="row">
  <div class="col-md-3 col-lg-3 col-md-offset-0 col-lg-offset-0 col-sm-offset-3 col-sm-6 col-xs-offset-3 col-xs-6 " >
  <br><br><br><br><br><br>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['category/index']);?>">MANAGE CATEGORY</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['subcategory/index']);?>">MANAGE SUBCATEGORY</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['product/index']);?>">MANAGE PRODUCTS</a></p>     
        
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['order/index']);?>">MANAGE ORDERS</a></p>
        
  </div>
   <div class="col-md-9 col-lg-9 col-md-offset-0 col-lg-offset-0 col-sm-offset-3 col-sm-6 col-xs-offset-3 col-xs-6 " >
<div class="tbl-product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Product', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'fk_category_int_id',
                'value'=>'fkCategoryInt.vchr_category_name',
            ],
               [
                'attribute'=>'fk_int_sub_category_id',
                'value'=>'fkIntSubCategory.vchr_sub_category_name',
            ],

            
            

           // 'fkIntSubCategory.vchr_sub_category_name',
            'vchr_product_name',
            'int_price',
            // 'vchr_product_image',
            // 'vchr_brand_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
</div>

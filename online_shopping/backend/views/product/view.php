<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TblProduct */

$this->title = $model->pk_product_id;
$this->params['breadcrumbs'][] = ['label' => ' Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pk_product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pk_product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pk_product_id',
            'fkCategoryInt.vchr_category_name',
            'fkIntSubCategory.vchr_sub_category_name',
            'vchr_product_name',
            'int_price',
            'vchr_product_image',
            'vchr_brand_name',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  backend\models\TblCategory;
use  backend\models\TblSubCategory;
use  backend\models\TblSizechart;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\TblProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'fk_category_int_id')->dropDownList(
            ArrayHelper::map(TblCategory::find()->asArray()->all(), 'pk_category_id', 'vchr_category_name')
            )?>
 <?= $form->field($model, 'fk_int_sub_category_id')->dropDownList(
            ArrayHelper::map(TblSubCategory::find()->asArray()->all(), 'pk_sub_category_id', 'vchr_sub_category_name')
            )?>
 
    

    <?= $form->field($model, 'vchr_product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'int_price')->textInput() ?>

   
     <?= $form->field($model, 'vchr_product_image')->fileInput() ?>

    <?= $form->field($model, 'vchr_brand_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'size[]')->checkboxList( ArrayHelper::map(TblSizechart::find()->asArray()->all(), 'pk_int_sizeid', 'vchr_size')); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

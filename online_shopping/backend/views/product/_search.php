<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pk_product_id') ?>

    <?= $form->field($model, 'fk_category_int_id') ?>

    <?= $form->field($model, 'fk_int_sub_category_id') ?>

    <?= $form->field($model, 'vchr_product_name') ?>

    <?= $form->field($model, 'int_price') ?>

    <?php // echo $form->field($model, 'vchr_product_image') ?>

    <?php // echo $form->field($model, 'vchr_brand_name') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

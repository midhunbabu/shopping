<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblCustomers */

$this->title = 'Update Tbl Customers: ' . $model->pk_int_customer_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk_int_customer_id, 'url' => ['view', 'id' => $model->pk_int_customer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-customers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblCustomers */

$this->title = 'Add Customers';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-customers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

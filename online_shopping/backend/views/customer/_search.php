<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-customers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pk_int_customer_id') ?>

    <?= $form->field($model, 'vchr_name') ?>

    <?= $form->field($model, 'vchr_gender') ?>

    <?= $form->field($model, 'vchr_email') ?>

    <?= $form->field($model, 'vchr_mobile_no') ?>

    <?php // echo $form->field($model, 'text_address') ?>

    <?php // echo $form->field($model, 'vchr_password') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

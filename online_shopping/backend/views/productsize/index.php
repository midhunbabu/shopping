<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsizeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Set Product Sizes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">

  <div class="row">
  <div class="col-md-3 col-lg-3 col-md-offset-0 col-lg-offset-0 col-sm-offset-3 col-sm-6 col-xs-offset-3 col-xs-6 " >
  <br><br><br><br>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['product/index']);?>">MANAGE PRODUCTS</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['productsize/index']);?>">MANAGE PRODUCT SIZES</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['category/index']);?>">MANAGE CATEGORY</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['subcategory/index']);?>">MANAGE SUBCATEGORY</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['order/index']);?>">MANAGE ORDERS</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['order-detail/index']);?>">MANAGE ORDER DETAILS</a></p>
        <p><a class="btn btn-lg btn-success" style="width: 100%;" href="<?=Url::to(['order-detail-status/index']);?>">MANAGE STATUS</a></p>
  </div>
  <div class="col-md-9 col-lg-9 col-md-offset-0 col-lg-offset-0 col-sm-offset-3 col-sm-6 col-xs-offset-3 col-xs-6 " >
<div class="tbl-product-size-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Product Size', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pk_size_id',
            'fk_item_id',
            'vchr_size',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
</div>
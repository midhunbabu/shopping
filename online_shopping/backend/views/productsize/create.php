<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblProductSize */

$this->title = 'Add Product Sizes';
$this->params['breadcrumbs'][] = ['label' => 'Add Product Sizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-product-size-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblOrderDetail */

$this->title = 'Create Tbl Order Detail';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Order Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-order-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

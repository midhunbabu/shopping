<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblOrderDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-order-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pk_int_order_detail_id')->textInput() ?>

    <?= $form->field($model, 'fk_int_order_id')->textInput() ?>

    <?= $form->field($model, 'fk_int_product_id')->textInput() ?>

    <?= $form->field($model, 'int_quantity')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

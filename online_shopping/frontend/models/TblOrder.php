<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "tbl_order".
 *
 * @property integer $pk_int_order_id
 * @property integer $fk_int_customer_id
 * @property string $date_date
 * @property integer $int_total_amount
 *
 * @property User $fkIntCustomer
 */
class TblOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_int_customer_id', 'date_date', 'int_total_amount'], 'required'],
            [['fk_int_customer_id', 'int_total_amount'], 'integer'],
            [['date_date'], 'safe'],
            [['fk_int_customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fk_int_customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_int_order_id' => 'Pk Int Order ID',
            'fk_int_customer_id' => 'Fk Int Customer ID',
            'date_date' => 'Date Date',
            'int_total_amount' => 'Int Total Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntCustomer()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_int_customer_id']);
    }
    /**
     *insert data into tbl_order
     *@param $userId stores the ID of user.
     *@param $date stores date of order.
     *@param $amount stores the total amount of order.
     *@return $currently created pk_int_order_id.
     */
    public function insertOrder($userId,$date,$amount)
    {   
        $this->fk_int_customer_id = $userId;
        $this->date_date = $date;
        $this->int_total_amount = $amount;
        $this->save();
        return $this->pk_int_order_id;
    }    
}

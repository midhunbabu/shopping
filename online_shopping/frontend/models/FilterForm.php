<?php
/**
* @author Ragisha P
* @link frontend\models\searchform.php
* @version 1.0
*
*/
namespace frontend\models;
use Yii;
use yii\base\Model;
/**
 * FilterForm represents the model behind the filter forms in search result page and product page
 *properties min for minimum price max for maximum price and brand for brand name
 */
class FilterForm extends Model
{
    
    public $min;
    public $max;
    public $brand;
    /**
    *rules set the rules for the form made with filter form model
    *@param no params
    *@return validations for form fields.
    */
    public function rules()
    {
        return [
                
                [['min','max'], 'integer','message'=>'please choose a fitering option'],
                [['brand'],'string']

            
        ];
    }

    

}
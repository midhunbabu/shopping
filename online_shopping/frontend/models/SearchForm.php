<?php
/**
* @author Ragisha P
* @link frontend\models\searchform.php
* @version 1.0
*
*/
namespace frontend\models;
use Yii;
use yii\base\Model;
/**
 * SearchForm represents the model behind the search form 
 */
class SearchForm extends Model
{
    public $keyword;
    public function rules()
    {
        return [
                [['keyword'], 'required'],
                ['keyword', 'string'],
            
        ];
    }

    /**
     *selects the products from the searched category
     *@param $keyword- is the search keyword received from input form(search)
     *@return all the products coming under the searched keyword.
     */  
    public function getdata($keyword)
    {
      return  TblProduct::find()
            ->joinWith('fkIntSubCategory')
            ->where(['OR',['like','tbl_sub_category.vchr_sub_category_name',$keyword],['like','tbl_product.vchr_product_name',$keyword]])
            ->joinWith('fkCategoryInt')
            ->asArray()
            ->all();
    }
}
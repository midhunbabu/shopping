<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
class ReviewForm extends Model
{   
	public $rating;
	public $reviewHead;
	public $reviewBody;
	public function rules()
    {
        return [
                [['reviewBody'], 'required'],
                ['reviewHead', 'string'],
            
        ];
    }
}
?>

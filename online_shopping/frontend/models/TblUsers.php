<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_users".
 *
 * @property integer $id
 * @property string $username
 * @property string $firstname
 * @property string $address
 * @property string $email
 * @property string $password
 */
class TblUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'firstname', 'address', 'email', 'password'], 'required'],
            [['address'], 'string'],
            [['username', 'firstname'], 'string', 'max' => 20],
            [['email', 'password'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'firstname' => 'Firstname',
            'address' => 'Address',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }
}

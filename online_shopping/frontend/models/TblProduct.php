<?php
/**
* @author Ragisha P Naveen Noble Midhun Babu
* @link frontend\views\shop\noresults.php
* @version 1.0
*
*/
namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_product".
 *
 * @property integer $pk_product_id
 * @property integer $fk_category_int_id
 * @property integer $fk_int_sub_category_id
 * @property string $vchr_product_name
 * @property integer $int_price
 * @property string $vchr_product_image
 *
 * @property TblOrderDetail[] $tblOrderDetails
 * @property TblCategory $fkCategoryInt
 * @property TblSubCategory $fkIntSubCategory
 * @property TblProductSize $tblProductSize
 */
class TblProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pk_product_id', 'fk_category_int_id', 'fk_int_sub_category_id', 'vchr_product_name', 'int_price', 'vchr_product_image'], 'required'],
            [['pk_product_id', 'fk_category_int_id', 'fk_int_sub_category_id', 'int_price'], 'integer'],
            [['vchr_product_name'], 'string', 'max' => 50],
            [['vchr_product_image'], 'string', 'max' => 20],
            [['fk_category_int_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblCategory::className(), 'targetAttribute' => ['fk_category_int_id' => 'pk_category_id']],
            [['fk_int_sub_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblSubCategory::className(), 'targetAttribute' => ['fk_int_sub_category_id' => 'pk_sub_category_id']],
            [['vchr_brand_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_product_id' => 'Pk Product ID',
            'fk_category_int_id' => 'Fk Category Int ID',
            'fk_int_sub_category_id' => 'Fk Int Sub Category ID',
            'vchr_product_name' => 'Vchr Product Name',
            'int_price' => 'Int Price',
            'vchr_product_image' => 'Vchr Product Image',
            'vchr_brand_name' => 'Vchr Brand Name ' 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblOrderDetails()
    {
        return $this->hasMany(TblOrderDetail::className(), ['fk_int_product_id' => 'pk_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkCategoryInt()
    {
        return $this->hasOne(TblCategory::className(), ['pk_category_id' => 'fk_category_int_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkIntSubCategory()
    {
        return $this->hasOne(TblSubCategory::className(), ['pk_sub_category_id' => 'fk_int_sub_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductSizes()
    {
        return $this->hasMany(TblProductSize::className(), ['fk_item_id' => 'pk_product_id']);
    }
    /**
     *selects the latest products from each category
     *@param $id- is the id of the category
     *@return latest products coming under the differentcategories
     */  
   
    public function getProduct($productId)
    {
        return TblProduct::find()
             ->where(['fk_int_category_id'=>$productId])
             ->joinWith('fkIntSubCategory')
             ->limit(4)
             ->asArray()
             ->all();
    }
    /**
     *selects the latest products from each category
     *@param $id- is the id of the category
     *@return latest products coming under the differentcategories
     */  

     public function getCartProduct($productId)
    {
        return TblProduct::find()
             ->where(['pk_product_id'=>$productId])
             ->joinWith('fkIntSubCategory')
             ->asArray()
             ->all();    
    }
    /**
     *selects the products from the selected category
     *@param $id- is the id of the category
     *@return all the products coming under the selected category.
     */  
     public function getData($id)
    {
        return  TblProduct::find()
        ->joinWith('fkCategoryInt')
        ->joinWith('fkIntSubCategory')
        ->where(['tbl_product.fk_category_int_id'=>$id])
        ->asArray()
        ->all();
    }
    /**
     *selects the products from the selected category
     *@param $id- is the id of the category
     *@param min defines minimum price of product
     *@param max defines maximum price of product
     *@param brand defines brand name of product
     *@return all the products coming under the selected category with selected filters.
     */  
    public function getDatas($id,$min,$max,$brand)
    {

         if($brand=="")
        {
               return  TblProduct::find()
        ->joinWith('fkCategoryInt')
        ->joinWith('fkIntSubCategory')
        ->where(['tbl_product.fk_category_int_id'=>$id])
        ->andWhere(['>', 'int_price', $min])
        ->andWhere(['<', 'int_price', $max])
        
        ->asArray()
        ->all();
        }
        else
        {
           return  TblProduct::find()
        ->joinWith('fkCategoryInt')
        ->joinWith('fkIntSubCategory')
        ->where(['tbl_product.fk_category_int_id'=>$id])
        ->andWhere(['>', 'int_price', $min])
        ->andWhere(['<', 'int_price', $max])
        ->andWhere(['tbl_product.vchr_brand_name'=>$brand])
        ->asArray()
        ->all(); 
        }
    }
     /**
     *selects the single product details
     *@param $id is the id of the selected product
     *@return full details of the selected product
     */ 
      public function getProductdata($id)
    {
        return  TblProduct::find()
        ->joinWith('fkCategoryInt')
        ->joinWith('fkIntSubCategory')
        ->joinWith('tblProductSizes')
        ->where(['tbl_product.pk_product_id'=>$id])
        ->asArray()
        ->all();
    }
    /**
     *selects the products from the searched category
     *@param $keyword- is the search keyword received from input form(search)
     *@param min defines minimum price of product
     *@param max defines maximum price of product
     *@param brand defines brand name of product
     *@return all the products coming under the searched keyword.
     */  
  
     public function getSearchdata($keyword,$min,$max,$brand)
    {
       if($brand=="")
       {
        return  TblProduct::find()
            ->joinWith('fkIntSubCategory')
            ->joinWith('fkCategoryInt')
            ->Where(['>', 'int_price', $min])
            ->andWhere(['<', 'int_price', $max])
            ->andWhere(['OR',['like','tbl_sub_category.vchr_sub_category_name',$keyword],['like','tbl_product.vchr_product_name',$keyword]])
            ->asArray()
            ->all();
       }
       else
       {
      return  TblProduct::find()
            ->joinWith('fkIntSubCategory')
            ->where(['>', 'int_price', $min])
            ->andWhere(['<', 'int_price', $max])
            ->andWhere(['tbl_product.vchr_brand_name'=>$brand])
            ->andwhere(['OR',['like','tbl_sub_category.vchr_sub_category_name',$keyword],['like','tbl_product.vchr_product_name',$keyword]])
            ->joinWith('fkCategoryInt')
            ->asArray()
            ->all();
        }
    }
     /**
     *selects the products from the searched category
     *@param $keyword- is the search keyword received from input form(search)
     *@return all the products coming under the searched keyword.
     */ 
 public function getSearchdatas($keyword)
    {
      return  TblProduct::find()
            ->joinWith('fkIntSubCategory')
            ->where(['OR',['like','tbl_sub_category.vchr_sub_category_name',$keyword],['like','tbl_product.vchr_product_name',$keyword],['like','tbl_product.vchr_brand_name',$keyword]])
            ->joinWith('fkCategoryInt')
            ->asArray()
            ->all();
    }
}

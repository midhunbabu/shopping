<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
$session = Yii::$app->session;    
$session->open();
if(isset($_SESSION['detail']))
    $cartSize=sizeof($_SESSION['detail']);
else
    $cartSize=0;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="logo.png" style="display:inline; vertical-align: top; height:230%; margin-top:-5%;">ONLINE SHOPPING',
        'brandUrl' => ['/shop/index'],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Men', 'url' => ['/shop/category','id'=>1]],
        ['label' => 'Women', 'url' => ['/shop/category','id'=>2]],
        ['label' => 'Kids', 'url' => ['/shop/category','id'=>3]],
    ];
    $menuItems2[] = '<li>'
            . Html::beginForm(['/shop/search'], 'get')
            .'<input type="text" name="text" placeholder="search" style="border-radius:6px; height:35px;width: 350px;">'
            . Html::submitButton(
                'search ',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    if (Yii::$app->user->isGuest) {
        $menuItems1[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems1[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems1[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    $menuItems3 = [
        ['label' => 'Cart('.$cartSize.')' , 'url' => ['/shop/cart']],

    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menuItems,
    ]);
     echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $menuItems2,
    ]);
     echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems1,
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems3,
    ]);

    NavBar::end();
    ?>

    <div class="container">
     
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"> shoppers-<?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

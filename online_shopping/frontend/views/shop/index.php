<?php 
/**
* @author Naveen Noble
* @link frontend\views\shop\index.php
* @version 1.0
*
*/
use yii\helpers\Url; 
?>  
<!DOCTYPE html>

<html>
<head>
    <title></title>
    <style type="text/css">
            .product:hover { -moz-box-shadow: 0 0 10px #ccc; -webkit-box-shadow: 0 0 10px #ccc; box-shadow: 0 0 10px #ccc; } 
    .product{border-right: 1px solid #ddd;}
    </style>
</head>
<body>
    <?php
    if(isset($status))
    {
    ?>
    <div class="alert alert-success">
    <strong>your order is successfull.thanks for shopping</strong>
    </div>
    <?php
    }

    ?>
    
    <?php
    
    foreach ($products as $key => $product) {
    ?>
    <div class="container-fluid">
        <div class="row ">
            <h3><?= $key.' '.'collections' ?></h3>      
            <?php   foreach($product as $itemKey=>$item)
            {
            ?>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 product">
                <center>
                <a href="<?=Url::to(['shop/singleproduct', 'id' => $item['pk_product_id']]);?>"><img src="http://files.baabtra.com/products/<?=$item['vchr_product_image']?>"  style="width: 100px; height: 150px;"></a>
                   <br><span style="font-size: 20px;"><a href="<?=Url::to(['shop/singleproduct', 'id' => $item['pk_product_id']]);?>"><?=$item['vchr_product_name']?></a><br><?="₹".$item['int_price']?></span>
                   </center>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php   
    }
    ?>

</body>
</html>



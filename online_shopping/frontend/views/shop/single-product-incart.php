<?php
/**
* @author Midhun Babu
* @link frontend\views\shop\singleproductincart.php
* @version 1.0
*
*/
use yii\helpers\Url;
use yii\helpers\html; 
use yii\helpers\ArrayHelper;
use yii\web\View;
?>

<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body  style="background-color: #F3F3F3;">
		<div class="container-fluid">
			<?php
				foreach ($data as $products) 
				{
			?>
			<div class="row" style="background-color: white;">
				<div class="col-md-5">
					<img src="http://files.baabtra.com/products/<?=$products['vchr_product_image']?>" style="width: 300px; height: 450px;">
				</div><br>
				<div class="col-md-6" style="margin-top: 2%">
					<h2 class="text-danger"><?=$products['vchr_product_name']?></h2><br>
					<h3 class="bg-warning text-info"><?="₹".$products['int_price']?></h3>
					<h4 class="bg-warning">Select Size</h4><br>

					<?php
						foreach ($products['tblProductSizes'] as $size) 
						{
					?>
					<form method="POST">
					<b><a class="btn btn-success btn-sm"><input type="radio" name="size" value="<?=$size['vchr_size']?>"><?=$size['vchr_size']?></a></b>
					<?php
						}
					?>
					</form>
					<h5><img src="tag.png" style="display:inline; width: 3%;">Special PriceExtra 10% Off On This Product</h5>
					<h5><img src="tag.png" style="display:inline; width: 3%;"><b>Bank</b> OfferExtra 5% off* on Axis Bank Buzz Credit Cards</h5>
					<h5><img src="tag.png" style="display:inline; width: 3%;"><b>Bank</b> OfferExtra 10% off* on Standard Chattered Bank Debit Cards</h5>
					<h5 style="color: red;"><img src="delivery.png" style="display:inline; width: 5%;"><b>Free Delivery Within 7 Days</b></h5>
				</div>
			</div>
			<div class="row" style="margin-top: 2%">
				<div class="col-md-2">
						<a href="<?=Url::to(['shop/cart'])?>"><input class="btn btn-success" type="button" name="cart" value="GO TO CART"></a>
				</div>
				<div class="col-md-2">
						<a href="<?=Url::to(['shop/index'])?>"><button class="btn btn-warning">Continue Shopping</button></a>
				</div>
			</div>
			<?php
				}
			?>
			<div class="row">
				<div class="col-md-6 col-md-offset-5" style="margin-top: -3%;">
					<a href="<?=Url::to(['shop/review'])?>"><button class="btn btn-success col-md-6">Rate And Review</button></a>
				</div>
			</div>
		</div>
	</body>
</html>


<?php
/**
* @author Midhun Babu
* @link frontend\views\shop\review.php
* @version 1.0
*
*/
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\Models\TblSubCategory;
use frontend\assets\AppAsset;

AppAsset::register($this);
$this->title = 'Review';
$this->params['breadcrumbs'][] = $this->title;
?>
<form id="ratingsForm">
	<div class="stars">
		<input type="radio" name="star" class="star-1" id="star-1" />
		<label class="star-1" for="star-1">1</label>
		<input type="radio" name="star" class="star-2" id="star-2" />
		<label class="star-2" for="star-2">2</label>
		<input type="radio" name="star" class="star-3" id="star-3" />
		<label class="star-3" for="star-3">3</label>
		<input type="radio" name="star" class="star-4" id="star-4" />
		<label class="star-4" for="star-4">4</label>
		<input type="radio" name="star" class="star-5" id="star-5" />
		<label class="star-5" for="star-5">5</label>
		<span></span>
	</div>
</form>

<?php $form = ActiveForm::begin(['id' => 'review-form' ]); ?>
<?= $form->field($model, 'reviewHead')->textInput() ?>
<?= $form->field($model, 'reviewBody')->textarea(['rows' => 6]) ?>
<?= Html::submitButton('Submit',['class' => 'btn btn-primary','name' => 'review-button']) ?>


<?php ActiveForm::end(); ?>




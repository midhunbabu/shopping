<?php 
/**
* @author Ragisha P 
* @link frontend\views\shop\search-result.php
* @version 1.0
*
*/
use yii\helpers\Url; 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\Models\TblProduct;
$this->title = 'Search Results';
?> 

<!DOCTYPE html>

<html>
<head>
    <title></title>
     <style type="text/css">
         .filter{
                 height: 100%px;
                }
         .product:hover {
                         -moz-box-shadow: 0 0 10px #ccc; -webkit-box-shadow: 0 0 10px #ccc; box-shadow: 0 0 10px #ccc; 
                        } 
         .product{
                    border-right: 1px solid #ddd;
                 }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row ">
         <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6 filter " style=" border-right:  solid #000; height:100vh;">

                <?php $form = ActiveForm::begin(['id' => 'filter-form']); ?>                                  
                <?php
                     $minvalues= ['0' => '0','500' => '500', '1000' => '1000','2000'=>'2000'];
                echo $form->field($model, 'min')->dropDownList($minvalues,array('options'=>array('0'=>array('selected'=>true)))); ?>
                    <?php
                     $maxvalues= ['500' => '500', '1000' => '1000','2000' => '2000','5000'=>'5000','10000'=>'10000'];
                echo $form->field($model, 'max')->dropDownList($maxvalues); ?>   
                     <?= $form->field($model, 'brand')->dropDownList(ArrayHelper::map(TblProduct::find()->all(), 'vchr_brand_name', 'vchr_brand_name'),['prompt' => 'select brand']);   ?>                       
                <div class="form-group">
                        <?= Html::submitButton('Apply filter',['class' => 'btn btn-primary', 'name' => 'search-button']) ?>
                </div> 

                <?php ActiveForm::end(); ?>
           </div>

            <?php
            if($products)

            {
              foreach ($products as $product)
            {
            ?>
                <div class="col-md-3 col-lg-3 col-md-offset-0 col-lg-offset-0 hidden-sm hidden-xs product ">
                    <center>
                    <a href="<?=Url::to(['shop/singleproduct', 'id' => $product['pk_product_id']]);?>"><img src="http://files.baabtra.com/products/<?=$product['vchr_product_image']?>" style="width: 100px; height: 150px;"></a><br> 
                    <a href="<?=Url::to(['shop/singleproduct', 'id' => $product['pk_product_id']]);?>"><span style="font-size: 20px;"><?=$product['vchr_product_name']?></a><br><?="₹".$product['int_price'] ?></span>
                    </center>
                </div>
            <?php
            }
            }
            else
            {
                ?>
                <div class="site-about">
                <center>
                   <img src="http://files.baabtra.com/products/empty.png" style="width: 350px; height: 225px;"><br>

                    <h1 style="margin-top: 15%;">Sorry..!! No results found !!</h1>
                    <p>Please check the spelling or try searching for something else</p>
                    <br>
                     <a href="<?=Url::to(['site/index']);?>">
                    <button class="btn-info" style="width:10%; padding: 5px;"">Try another?</button></a>
                </center>
                </div>
                <?php
            }
                ?>
            </div>
        </div>
   

</body>
</html>




 



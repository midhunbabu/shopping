<?php 
/**
* @author Naveen Noble
* @link frontend\views\shop\cart.php
* @version 1.0
*
*/
use yii\helpers\Url;
use yii\helpers\html; 
use yii\helpers\ArrayHelper;
use yii\web\View;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cart</title>      
    </head>
    <body style="background-color:#F3F3F3">
        <div class="container-fluid" >
            <div class="row " >
           
                <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12" style= "background-color:#FFFFFF">
                    <h4><strong>MY CART(<?=$count?>)</strong></h4>
                    
                    <div class="container-fluid">
                        <div class="row ">
                            <!-- This loop is to itreate through each product in the cart  -->
                            <?php foreach ($product as $key => $items) 
                            {
                            ?> 
                              
                            
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="background-color:#FFFFFF; border-top:6px">
                                    <div class="container-fluid">
                                        <div class="row " style="margin-bottom:20px; ">
                                            <!-- this loop is used to iterate throuh $items which is also in array format-->
                                            <?php   foreach($items as $itemKey=>$item)
                                            {
                                            ?>
                                            <hr>
                                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 ">    
                                                    <img src="http://files.baabtra.com/products/<?=$item['vchr_product_image']?>" style="width: 75px; height: 100px;">
                                                    
                                                    
                                                                                             
                                                </div>
                                                <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12 " style="padding-left: 20px" >
                                                    <!--This loop is used to iterate through session in order to get the qantity of each item -->
                                                    <?php
                                                    foreach ($_SESSION['detail'] as $key => $value) {
                                                        if($_SESSION['detail'][$key]['id']==$item['pk_product_id'])
                                                        {
                                                        ?>                                                            
                                                    <strong><?=$item['vchr_product_name']?></strong><br><?=$item['fkIntSubCategory']['vchr_sub_category_name'] ?><br><h5><strong>RS:<?=$item['int_price']*$_SESSION['detail'][$key]['qty']?>
                                                    </strong></h5>
                                                    
                                                    
                                                    <div>
                                                        <h5>Select Quantity</h5>
                                                        <a href="<?=Url::to(['shop/cart', 'qty_plus' => $item['pk_product_id']]);?>">
                                                            <button class="btn-primary">+</button>
                                                        </a>
                                                         
                                                        <?php
                                                            echo $_SESSION['detail'][$key]['qty'];  
                                                        }
                                                    }
                                                    ?>
                                                        <a href="<?=Url::to(['shop/cart', 'qty_minus' => $item['pk_product_id']]);?>"><button class="btn-primary">-</button></a>
                                                        <a href="<?=Url::to(['shop/cart', 'id' => $item['pk_product_id']]);?>"><strong><h4 style="color:#000000">remove</h4></strong></a>
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            
                                        </div>
                                    </div>  
                                </div>
                            <?php
                            } 
                            ?>
                            
                        </div>
                        <strong><hr></strong>
                    </div>
                    <a href="<?=Url::to(['shop/index']);?>"><button class="btn-success" style="width:25%; padding: 15px; ">continue shopping</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?=Url::to(['shop/checkout']);?>">
                    <button class="btn-info" style="width:25%; padding: 15px;"">Place Order</button></a>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 " style= "background-color:#FFFFFF;margin-left:20px">
                    <p><h4><strong>PRICE DETAILS</strong></h4></p>
                    <hr>
                    <h5>Items:<?= $count ?></h5>
                    <h5>Amount:<?=$sum ?><h5>
                    <h5>Delivary Charge:Rs 0.00</h5>
                    <hr>
                    <h5><strong>Total Amount:Rs <?=$sum ?></strong></h5>
                </div>
            </div>
        </div>

    </body>
</html>
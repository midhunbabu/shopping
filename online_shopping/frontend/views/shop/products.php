<?php 
/**
* @author Ragisha P 
* @link frontend\views\shop\products.php
* @version 1.0
*
*/
use yii\helpers\Url; 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\Models\TblProduct;
use yii\web\View;
$this->title = 'products';
?> 

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <style type="text/css">
        .filter{
            height: 100%px;
        }
        .product{border-right: 1px solid #ddd;}
        .product:hover { -moz-box-shadow: 0 0 10px #ccc; -webkit-box-shadow: 0 0 10px #ccc; box-shadow: 0 0 10px #ccc; } 
    </style>
</head>
<body>

 
    <div class="container-fluid">
        <div class="row ">
               <div class="col-md-2 col-lg-2 col-sm-6 col-xs-6 filter " style=" border-right:  solid #ff0000; height:100vh;">

                <?php $form = ActiveForm::begin(['id' => 'filter-form']); ?>
               <?php   
                    $value=$_GET['id'];?>
                                                    
                <?php
                     $minvalues= ['0' => '0','500' => '500', '1000' => '1000','2000'=>'2000'];
                echo $form->field($model, 'min')->dropDownList($minvalues,array('options'=>array('0'=>array('selected'=>true)))); ?>
                    <?php
                     $maxvalues= ['500' => '500', '1000' => '1000','2000' => '2000','5000'=>'5000','10000'=>'10000'];
                echo $form->field($model, 'max')->dropDownList($maxvalues,['10000'=>'10000']); ?>   
                 <?= $form->field($model, 'brand')->dropDownList(ArrayHelper::map(TblProduct::find()->where(['tbl_product.fk_category_int_id'=>$value])->all(), 'vchr_brand_name', 'vchr_brand_name'),
            ['prompt' => 'select brand']);   ?>     

                <div class="form-group">
                        <?= Html::submitButton('apply filter',['class' => 'btn btn-primary', 'name' => 'search-button']) ?>
                    </div> 

                <?php ActiveForm::end(); ?>
               
               </div>
               
            <?php 

            if($products)

            {

               foreach ($products as $product)
              {
              ?>
                  <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 product" >
                      <center>
                      <a href="<?=Url::to(['shop/singleproduct', 'id' => $product['pk_product_id']]);?>"><img src="http://files.baabtra.com/products/<?=$product['vchr_product_image']?>" style="width: 100px; height: 150px;"></a>
                      <br> <span style="font-size: 20px;"><a href="<?=Url::to(['shop/singleproduct', 'id' => $product['pk_product_id']]);?>"><?=$product['vchr_product_name']?></a><br><?="₹".$product['int_price'] ?></span>

                      <button type="button" id="b1" style="display: none;">Hello</button>
                      </center>
                  </div>
            <?php
              }
            }
            else
            {
                ?>
                <div class="site-about">
                  <center>
                     
                      
                      <img src="http://files.baabtra.com/products/empty.png" style="width: 350px; height: 225px;"><br>
                      <h1 style="margin-top: 15%;">Sorry..!! No results found !!</h1>
                      <br>
                      <a href="<?=Url::to(['site/index']);?>">
                      <button class="btn-info" style="width:10%; padding: 5px;"">Try again?</button></a>
                  </center>
                </div>
            <?php
            }
            ?>
            </div>
        </div>
</body>
</html>




 



<?php 
/**
* @author Naveen Noble
* @link frontend\views\shop\checkout.php
* @version 1.0
*
*/
use yii\helpers\Url;
use yii\helpers\html; 
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\bootstrap\ActiveForm;


?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
<?php
$this->registerJs("
    $(document).ready(function(){
    $('#flip').click(function(){
        $('#panel').slideToggle('slow'  );
    });
});",
View::POS_READY,
'my-button-handler');
?>
<?php
$this->registerJs("
    $(document).ready(function(){
    $('#flip1').click(function(){
        $('#panel1').slideToggle('slow');
    });
});",
View::POS_READY,
'my-button-handler1');
?>
<?php
$this->registerJs("
    $(document).ready(function(){
    $('#flip2').click(function(){
        $('#panel2').slideToggle('slow');
    });
});",
View::POS_READY,
'my-button-handler2');
?>
<?php
$this->registerJs("
    $(document).ready(function(){
    $('#ctn').click(function(){
        $('#panel2').hide('slow');
        $('#panel1').hide('slow');
        $('#panel').hide('slow');
        $('#panel4').show('slow');
    });
});",
View::POS_READY,
'my-button-handler3');
?>
<style type="text/css">
.flip
{
    background-color:#CCCCCC;
    width:100%;
    height:60px;
}
.btnn
{
    background-color: #FC8332;
    width:150px;
    height: 50px;
}
    
</style>

</head>
<body style= "background-color:#FFFFFF">
<div class="container-fluid" >
    <div id="flip" class="row flip"  >
    <br>

           
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" >
        
            <strong>1.LOGIN ID</strong>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" >
        
            <strong><p><?= $userEmail ?></strong></p>
        </div>
    </div>  

    <div id="panel" class="row"  style="display:none;"><br>
        <h4>logged in as <?= $userEmail ?></h4>
        <a href="<?=Url::to(['shop/signout']);?>"><button class="btnn">SignOut</button></a>
    </div>
    <br>
        <div>   
    </div>  
    <div id="flip1" class="row flip" >
           
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" ">
            <strong>2.DELIVERY ADDRESS </strong>
                       
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" ">
            <strong><?= $userName ?></strong><br>
                       <p><?=$userAddress ?></p>
        </div>
    </div>    
    <div id="panel1" class="row"  style="display:none;"><br>
    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" ">
        <?php $form = ActiveForm::begin(['id' =>'review-form']); ?>
        <?= $form->field($address, 'address')->textarea(['value'=>$userAddress]) ?>
        <?= Html::submitButton('Submit',['class' => 'btn btn-primary','name' => 'review-button']) ?>
    <?php ActiveForm::end(); ?>
    </div>
    </div>
    <br>
    <div id="flip2" class="row flip">   
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" >          
            <p><strong>3.Order Details</strong>&nbsp;&nbsp;<?=$count ?>&nbsp;Items</p>
        </div>
    </div> 
    <div id="panel2" >   
    <div  class="row">  
        <div class="col-md-5 col-lg-3 col-sm-5 col-xs-5" style= "background-color:#FFFFFF">          
            <CENTER><strong>Item</strong></CENTER>
        </div>
        <div class="col-md-1 col-lg-2 col-sm-1 col-xs-1" style= "background-color:#FFFFFF">          
            <center><strong>Qty</strong></center>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" style= "background-color:#FFFFFF">          
            <center><strong>Price</strong></center>
        </div>
        <div class="col-md-2 col-lg-3 col-sm-2 col-xs-2" style= "background-color:#FFFFFF">          
            <center><strong>Delivary Details</strong></center>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" style= "background-color:#FFFFFF">          
            <center><strong>Sub Total</strong></center>
        </div>
        <hr>
    </div>    
        <!-- This loop is to itreate through each product in the cart  -->
    <?php foreach ($product as $key => $items) 
          {
     ?> 


    <div class="row">   
          
        <div class="col-md-3 col-lg-3 col-sm-5 col-xs-5" style= "background-color:#FFFFFF">
            <div class="col-md-5 col-lg-5 col-sm-2 col-xs-2">     
                <img src="http://files.baabtra.com/products/<?=$items[0]['vchr_product_image']?>" style="width: 75px; height: 100px; margin-left: 10px;">
            </div>  
            <div class="col-md-6 col-lg-6 col-sm-3 col-xs-3" style="margin-left:10PX" >     
                <strong><center><?=$items[0]['vchr_product_name']?></center></strong>
                <center><?=$items[0]['fkIntSubCategory']['vchr_sub_category_name'] ?></center>
                <center>Size:xl</center>
                <center>Seller:Ecatr</center>
            </div>         
        </div>
        <div class="col-md-2 col-lg-2 col-sm-1 col-xs-1" style= "background-color:#FFFFFF">
            <CENTER> <?= $_SESSION['detail'][$key]['qty'] ?></CENTER>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-1 col-xs-1" style= "background-color:#FFFFFF">
            <CENTER> <?= $items[0]['int_price'] ?></CENTER>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-1 col-xs-1" style= "background-color:#FFFFFF">
            <CENTER><strong>Deliverd Within 7 days</strong> </CENTER>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-1 col-xs-1" style= "background-color:#FFFFFF">
            <CENTER><strong>RS:<?=$items[0]['int_price']*$_SESSION['detail'][$key]['qty']?>
                                            </strong> </CENTER>
        </div>
    </div>
           <br><br>
    <?php
    }
    ?>
            
        <div class="row">    
            <div style="width:30%" class="col-md-3 col-lg-3 col-sm-1 col-xs-1"><button class="btnn" id="ctn">Continue</button></div>
            <div style="width:30%" class="col-md-6 col-lg-6 col-sm-1 col-xs-1"></div>
                  
            <div style="width:30%" class="col-md-3 col-lg-3 col-sm-1 col-xs-1"><h2>Total Amount:&nbsp;<?=$sum ?></h2> </div> 
        </div>       

    </div>
    <br>
    <div id="flip4" class="row flip" >
           
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" ">
            <strong>4.PAYMENT METHODE</strong>
                       
        </div>
        
    </div>

    <div id="panel4" class="row"  style="display:none;"><br>
    <form>
    <input type="radio" name="radio" checked="true">Master card
    <input type="radio" name="radio">Visa
    <input type="radio" name="radio">Rupay
    
    </form>
    <a href="<?=Url::to(['shop/order']);?>"><button class="btnn">Confirm order</button></a>
        
    </div>
</div>

                 
    
    
</div>
</body>
</html>

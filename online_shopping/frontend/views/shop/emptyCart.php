<?php 
/**
* @author Naveen Noble
* @link frontend\views\shop\emptycart.php
* @version 1.0
*
*/
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cart</title>      
    </head>
    <body>
        <div class="container-fluid">
            <div class="row ">
           
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
	                <br>
	                <p>MY CART(0)</p>
	                <hr><br><br>
	               		<center>
	               			<img src="http://files.baabtra.com/products/empty.png" style="width: 350px; height: 225px;"><br>
	               			<h2>Your Cart is empty!!</h2>

	               		</center>

                </div>

            </div>    
        </div>
    </body>
</html>            
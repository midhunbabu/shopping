<?php
/**
* @author Midhun Babu Naveen Noble Ragisha P
* @link frontend\controllers\shopcontroller.php
* @version 1.0
*
*/
namespace frontend\controllers;
use frontend\models\SearchForm;
use frontend\models\FilterForm;
use frontend\models\TblCategory;
use frontend\models\TblProduct;
use frontend\models\TblReview;
use frontend\models\ReviewForm;
use frontend\models\TblOrder;
use frontend\models\TblOrderDetail;
use frontend\models\TblOrderDetailStatus;
use frontend\models\AddressForm;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\helpers\Url; 

/**
* ShopController implements the  actions for loading index page,product views,cart and  searching.
*/
class ShopController extends Controller
{
    /**
     *loads index page along with data.
     *@return index page along with data.
     */

    public function actionIndex()
    {
        $modelCategory= new TblCategory;
        $modelProducts= new TblProduct;
        $categories=$modelCategory->getCategory();
        foreach ($categories as $category)
        {
            $products[$category['vchr_category_name']]=$modelProducts->getProduct($category['pk_category_id']);
        }                 
        return $this->render('index', ['products'=>$products]);
       
    }

    /**
     *performs search action 
     *receives 'text' -search keyword as input from search in header  
     *loads search page with the search result
     *contains filter function to filter the search results based on price and brand
     *@param no parameters
     *@return search-result page along with result data.
     */
     public function actionSearch()
    {
        $model = new FilterForm;
        $modelProducts= new TblProduct;
        $searchkey=Yii::$app->request->get('text');
        if($searchkey)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())//If filter is applied
            {         
                $products = $modelProducts->getSearchdata($searchkey,$model->min,$model->max,$model->brand);
                return $this->render('search-result',['products' => $products,'model'=>$model]);
            }

            else //filter is not applied
            {
                $products = $modelProducts->getSearchdatas($searchkey);
                return $this->render('search-result',['products' => $products,'model'=>$model]); 
            }
        }
        else
        {
            return $this->render('noresults');
        }
    }
    /**
     *performs  cart operations 
     *loads cart page with the products added to cart
     *@return cart page along with total amount and selected products.
     */
    public function actionCart()
    {
        $count=0;
        $sum=0;
        $session = Yii::$app->session;
        
        
        $session->open();
    //Id of the item to be removed is recived in get methode

        if(Yii::$app->request->get('id'))
        {
            
            $id=Yii::$app->request->get('id');
            if(!is_numeric($id))
            {
                throw new Exception('Invalid id value.');
                 
            }

        //A search is done for corresponding id and when found it is unset from the session       
            foreach ($_SESSION['detail'] as $key => $value)
            {
                if($session['detail'][$key]['id']==$id)
                {
                    unset($_SESSION['detail'][$key]);
                }

            }
        }

        //Id of the item to be whose qty to be increased is recived in get methode
        if (Yii::$app->request->get('qty_plus'))
        {
            
            $id=Yii::$app->request->get('qty_plus');
            if(!is_numeric($id))
            {
                throw new Exception('Invalid id value.');
                 
            }
        //A search is done for corresponding id and when found its qty is incremented from the session    
            foreach ($_SESSION['detail'] as $key => $value) 
            {
               
                if($value['id']==$id)
                {
                    $_SESSION['detail'][$key]['qty']++;
                    $this->redirect(Url::to(['shop/cart']));
                
                }

            }
            
        }
        //Id of the item to be whose qty to be decreased is recived in get methode
        if(Yii::$app->request->get('qty_minus'))
        {
            
            $id=Yii::$app->request->get('qty_minus');
            if(!is_numeric($id))
            {
                throw new Exception('Invalid id value.');
                 
            }
        //A search is done for corresponding id and when found its qty is decremented from the session    
            foreach ($_SESSION['detail'] as $key => $value) 
            {
               if($session['detail'][$key]['id']==$id && $_SESSION['detail'][$key]['qty']>0)
               {
                    $_SESSION['detail'][$key]['qty']--;
                    $this->redirect(Url::to(['shop/cart']));
               }

            }
            
        }
         
        $ids = $session->get('detail');
        if(!empty($ids))
        {    

            $modelProducts= new TblProduct;
            //Traversal through session is done in order to get the total amount of items stored in the cart.  
            foreach ($ids as $key => $id)
            {

                $product[]=$modelProducts->getCartProduct($id['id']);
                
                $sum=$sum + ($product[$count][0]['int_price']*$id['qty']);
                
                $count++;
            
            }  

            return $this->render('cart', ['product' => $product,'count'=>$count,'sum'=>$sum]);
        }
        else
            return $this->render('emptyCart');
        
        
        
    }
    /**
     *selects the products in the specific category and displays the  products in products page.
     *receives id ie id of the category selected by user.
     *contains filter function to filter the products based on price and brand
     *@param no parameters.
     *@return product page along with result data.
     */
    public function actionCategory()
    {
        $id=Yii::$app->request->get('id');
        if(!is_numeric($id))
        {
            throw new Exception('Invalid id value.');        
        }      
        else
        {
            $modelProducts= new TblProduct;
            $model = new FilterForm;
            if ($model->load(Yii::$app->request->post()) && $model->validate())//if filter is applied
            {      
                $products = $modelProducts->getDatas($id,$model->min,$model->max,$model->brand);
                return $this->render('products',['products' => $products,'model'=>$model]);
            }
            else// if no filters applied
            {
                $products = $modelProducts->getData($id);
                return $this->render('products',['products' => $products,'model'=>$model]); 
            }
        }
    }

    /**
     *selects the single product details
     *displays the  details of a product in a page
     *id receives the id of the selected product
     *@return single-product page along with result data.
     */   
   public function actionSingleproduct()
    {
        $session = Yii::$app->session;
        $session->open();
        
        $id=Yii::$app->request->get('id');
        if(!is_numeric($id))
        {
            throw new Exception('Invalid id value.');
                 
        }
        $modelProducts= new TblProduct;
        $products['data'] = $modelProducts->getProductdata($id);
        if(isset($_SESSION['detail']))
        {    
            foreach ($_SESSION['detail'] as $key => $value)
                {

                    if($value['id']==$id)
                    {

                        return $this->render('single-product-incart',$products);
                    }

                }
        }        
        
        return $this->render('single-product',$products);
    }

    /**
     *selects the single product details
     *displays the  details of a product in a page
     *product_id receives the id of the selected product
     *@return single-product page along with result data.
     */  
    public function actionCartvalues()
    {
        $product_id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $_SESSION['detail'][] = array('id' =>  $product_id, 'qty' => 1);
        $this->redirect(Url::to(['shop/cart']));
    }
     
    /**
     *customer feedback form is created
     *stores reviw in database
     *@return review pageno params
     *@param 
     */    

    public function actionReview()
    {
        $session = Yii::$app->session;         
        $session->open();
        
        if(isset($_SESSION['__id']))
        {    
                  
            $model = new ReviewForm;
            $ratemodel=new TblReview;
            
            if($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $user_id=$_SESSION['__id'];
                $ratemodel->insertReview($model->reviewHead,$model->reviewBody,$user_id);
                $this->redirect(Url::to(['shop/index']));
            }  
             return $this->render('review',['model'=>$model]); 
        }
        else
        {
            $this->redirect(Url::to(['site/login']));

        }     
               
    }   

     /**
     *places the order
     *displays the  details of a products and user in a page
     *allows final confirmation and submission of product
     *@return details of product in cart and loged in user
    */ 


   
     /**
     *places the order
     *displays the  details of a products and user in a page
     *allows final confirmation and submission of product
     *@return details of product in cart and loged in user
    */ 


    public function actionCheckout()
    {
        $session = Yii::$app->session;         
        $session->open();
        $modelProducts= new TblProduct;
        $count=0;
        $sum=0;
        //check whether the user is logged in or not        
        if(isset($_SESSION['__id']))
        { 
            $address= new AddressForm;
            $id=$_SESSION['__id'];
        //if the edited address is passed as post method the data base is updated with new 
        // address    
            if($address->load(Yii::$app->request->post()) && $address->validate())
            {
                $add=$address->address;
              

                $command = Yii::$app->db->createCommand("update user set address='$add' where id='$id'")->execute();
              
             
            }
        //Details of each user is takken from the session and is passed to the view.
            $user_id=$_SESSION['__id'];
            $user=User::find()->where(['user.id'=>$user_id])->asArray()->all();
            
            $userName=$user[0]['username'];
            $userAddress=$user[0]['address'];
            $userMobile=$user[0]['mobile'];
            $userEmail=$user[0]['email'];

            $ids = $session->get('detail');
        //Total amount is calculated here and passed to the view    

            foreach ($ids as $key => $id)
            {

                $product[]=$modelProducts->getCartProduct($id['id']);
                
                $sum=$sum + ($product[$count][0]['int_price']*$id['qty']);
                
                $count++;
            
            }

            return $this->render('checkout',['userName'=>$userName,'userAddress'=>$userAddress,'userMobile'=>$userMobile,'userEmail'=>$userEmail,'product'=>$product,'sum'=>$sum,'count'=>$count,'user_id'=>$user_id,'address'=>$address]);
               
        }
                         
        else
        {
            $this->redirect(Url::to(['site/login']));

        } 
    }
    /**
     *Signout the user
     *@return home page
    */ 
    public function actionSignout()
    {   

        Yii::$app->user->logout();

        return $this->goHome();
      

    }
    /**
     *Stores the order placed by the user into tbl_order,
     *tbl_order_detail and tbl_order_detail_status from session
     *@param no param.
     *@return confirmation page
     */
    public function actionOrder()
    {

        $session = Yii::$app->session;         
        $session->open();
        //checks if the user is logged in or not.
        if(isset($_SESSION['__id']))
        {
            $user_id=$_SESSION['__id'];
            $date=date('Y-m-d');
            $ids = $session->get('detail');
            $sum=0;
            $count=0;
            if(!empty($ids))
            {      
                $modelProducts= new TblProduct;
                //total amount is caluculated to insert into data base.
                foreach ($ids as $key => $id)
                {

                    $product[]=$modelProducts->getCartProduct($id['id']);
                    
                    $sum=$sum + ($product[$count][0]['int_price']*$id['qty']);
                    $count++;     
                
                }  

            }
            
            $order=new TblOrder;
            $order_detail=new TblOrderDetail;
            $order_status=new TblOrderDetailStatus;
            $lastId=$order->insertOrder($user_id,$date,$sum);
            //detail of each item is inserted into database table order detail 
            //and order detail status
            foreach ($ids as $key => $id)
            {
                $product_id=$id['id'];
                $product_qty=$id['qty'];
                $orderId =  $order_detail->insertOrderDetail($lastId,$product_id,$product_qty);
                $status=$order_status->insertOrderstatus($lastId,$orderId,$date);

            }
            
                $status=1;
                $modelCategory= new TblCategory;
                $modelProducts= new TblProduct;
                $categories=$modelCategory->getCategory();
                foreach ($categories as $category)
                {
                    $products[$category['vchr_category_name']]=$modelProducts->getProduct($category['pk_category_id']);
                }                 
                return $this->render('index', ['products'=>$products,'status'=>$status]);
       

                



           

        }



    }
    

}
?>